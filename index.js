const cron = require("node-cron");
const fs = require("fs");
var Module = require('module');
const shell = require('shelljs');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const codes_session = require("./codeAJC.json").codes_session;
const student = require("./student.json");

const civilite = student.civilite;
const nom =  student.nom;
const prenom =  student.prenom;
const mail = encodeURIComponent(student.mail);
const entreprise = student.entreprise;
const photo = student.photo;
const filiere = student.filiere;
const pc = student.pc;
const distance = student.distance;
const urlencoded = encodeURIComponent(`${civilite};${nom};${prenom};${mail};${entreprise};${photo};${distance}`)

const unused_signatures_path = './img/unused/';
const used_signatures_path = './img/used/';

var today = new Date(Date.now());
var currentDate = today.toISOString().substring(0, 10);
var periode = today.getHours()+2 < 12 ? 1 : 2;//Fuseau horaire utc+2 (heure d'été)

var schoolId = "v4qawzkhpog2ym5";
var courseId = "";
var studentId = "vj2d7xz7dk90js";
var verificationToken = "";


var code = "";
var session = "";

/*
 Permet d'encoder une image en base64 en l'important avec require()
*/
Module._extensions['.png'] = function(module, fn) {
  var base64 = fs.readFileSync(fn).toString('base64');
  module._compile('module.exports="data:image/png;base64,' + base64 + '"', fn);
};

var unused_signatures_directory = fs.readdirSync(unused_signatures_path);

/*
  Verifie que unused signatures n'est pas vide. Si il est vide, déplace le contenu de used dans unused
*/
async function checkUnusedSignatureDir() {
  unused_signatures_directory = fs.readdirSync(unused_signatures_path);
  if (unused_signatures_directory.length == 0) {
    shell.mv(used_signatures_path + '*', unused_signatures_path)
    unused_signatures_directory = fs.readdirSync(unused_signatures_path);
    console.log(`${new Date(Date.now()).toISOString()}: Dossier de signature unused vide. Le contenu de used a été redéplacé vers unused.`);
  }
}

/*
  Déplace la signature utilsée vers used
*/
async function moveSignaturetoUsed(chosenSignature) {
  return await shell.mv(unused_signatures_path + chosenSignature, used_signatures_path);
}

/*
  Selectionne une signature aléatoire du dossier unused, la deplace vers used, et renvoie la signature encodée en base64
*/
function selectSignatureImg() {
  return checkUnusedSignatureDir().then(() => {
    let chosenSignature = unused_signatures_directory[Math.floor(Math.random() * unused_signatures_directory.length)];
    return chosenSignature;
  }).then((chosenSignature) => {
    encodedSignature = moveSignaturetoUsed(chosenSignature).then(() => {
      let encodedSignature = require(used_signatures_path + chosenSignature);
      return encodedSignature;
    })
    console.log(`${new Date(Date.now()).toISOString()}: Signature selectionnée. ${chosenSignature} encodée en base64 et déplacée vers used.`)
    return encodedSignature;
  })
}

/*
  Renvoie la valeur d'un paramètre en query de l'url
*/
function getParameterByName(name, url) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/*
  Met à jour la variable currentDate,
  periode (si matin ou aprem),
  et le code quest AJC
*/
function resolveDateBasedValues() {
  today = new Date(Date.now())
  currentDate = today.toISOString().substring(0, 10);
  periode = today.getHours()+2 < 12 ? 1 : 2;
  code = codes_session.filter((course) => {
    if (new Date(course.start).getTime() < today.getTime()) {
      if (new Date(course.end).setHours(23, 59) > today.getTime()) {
        return true;
      }
    }
    return false;
  })[0].code
  console.log(`${today.toISOString()}: Actualisation  des varaiables dépendantes de la date.
  Un envoi de signature va être effectuer dans les 10 prochaines minutes.
  Le code AJCQuest est ${code}. La periode est ${periode}.`)
}


/*
  Procédure d'envoie de signature
  1.POST "https://quest.ajc.razon.fr/2/index.php" pour récupérer le code session et verifie le code AJC Quest

  2.POST "https://quest.ajc.razon.fr/2/saisiepresence.php" saiser du formulaire sur AJC Quest et retourne le lien Edusign dont on recupere courseId,studentId et verificationToken

  (2.5 Get GetCourse effectué sur la page Edusign, surement facultative, pour vérifier si on a déja signé, aucune donnée intérressante pour l'envoi de signature, si ce n'est schoolId mais qui doit être un Id fixe, permet de voir qui a déja signé sur le cours,  acces refusé si déja signé )

  3.OPTION "https://api.edusign.fr/student/courses/email/setStudentPresent/..."  Preflight request sur Edusign

  4.POST "https://api.edusign.fr/student/courses/email/setStudentPresent/..." Signature sur edusign
*/
function sendSignature() {
  console.log(`${new Date(Date.now()).toISOString()}: Procédure d'envoi de signature initialisé.`)
  //POST code AJC Quest
  fetch("https://quest.ajc.razon.fr/2/index.php", {
    "headers": {
      "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
      "cache-control": "max-age=0",
      "content-type": "application/x-www-form-urlencoded",
      "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"100\", \"Google Chrome\";v=\"100\"",
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": "\"Windows\"",
      "sec-fetch-dest": "frame",
      "sec-fetch-mode": "navigate",
      "sec-fetch-site": "same-origin",
      "sec-fetch-user": "?1",
      "upgrade-insecure-requests": "1"
    },
    "referrer": "https://quest.ajc.razon.fr/2/index.php",
    "referrerPolicy": "strict-origin-when-cross-origin",
    "body": `dateconnexion=${currentDate}&codeeval=${code}&submit=Valider`,
    "method": "POST",
    "mode": "cors",
    "credentials": "omit"
  }).then(res => res.text()).then(html => {
    let dom = new JSDOM(html);
    session = dom.window.document.querySelector("input[name='session']").value;
    console.log(`${new Date(Date.now()).toISOString()}: Code AJCQuest validé. data={"session":"${session}"}`)
  }).then(() => {
    //POST formulaire presence sur AJC Quest
    fetch("https://quest.ajc.razon.fr/2/saisiepresence.php", {
      "headers": {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
        "cache-control": "max-age=0",
        "content-type": "application/x-www-form-urlencoded",
        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"100\", \"Google Chrome\";v=\"100\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "frame",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "same-origin",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1"
      },
      "referrer": "https://quest.ajc.razon.fr/2/saisiepresence.php",
      "referrerPolicy": "strict-origin-when-cross-origin",
      "body": `dateconnexion=${currentDate}&codeeval=${code}&session=${session}&filiere_ii=${urlencoded}&civilite=${civilite}&nom=${nom}&prenom=${prenom}&email=${mail}&entreprise=${entreprise}&filiere=${filiere}&periode=${periode}&distance=${distance}&pc=${pc}&submit=Signer`,
      "method": "POST",
      "mode": "cors",
      "credentials": "omit"
    }).then(res => res.text()).then(html => {
      //Recuperation du lien Edusign
      let dom = new JSDOM(html);
      let link = dom.window.document.querySelectorAll('a')[5].href;
      courseId = getParameterByName('courseId', link);
      verificationToken = getParameterByName('verificationToken', link);
      studentId = getParameterByName('studentId', link);
      console.log(`${new Date(Date.now()).toISOString()}: Envoi du formulaire sur AJCQuest réussi. data={"verificationToken"="${verificationToken}","studentId":"${studentId}","courseId":"${courseId}","urlEdusign"="${link}"}`)

      //Get course Edusign
      fetch(`https://api.edusign.fr/student/courses/email/getCourse/${courseId}/${studentId}?verificationToken=${verificationToken}`, {
        "headers": {
          "accept": "application/json, text/plain, */*",
          "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
          "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"100\", \"Google Chrome\";v=\"100\"",
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": "\"Windows\"",
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site"
        },
        "referrer": "https://static.edusign.fr/",
        "referrerPolicy": "strict-origin-when-cross-origin",
        "body": null,
        "method": "GET",
        "mode": "cors",
        "credentials": "omit"
      }).then(res=>res.json()).then(json=>{
        console.log(`${new Date(Date.now()).toISOString()}: Get course sur Edusign. data=${JSON.stringify(json.result)}`);
        schoolId=json.result.SCHOOL_ID;
      });

      //Preflight signature Edusign
      fetch(`https://api.edusign.fr/student/courses/email/setStudentPresent/${courseId}?verificationToken=${verificationToken}`, {
        "headers": {
          "accept": "*/*",
          "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site"
        },
        "referrer": "https://static.edusign.fr/",
        "referrerPolicy": "strict-origin-when-cross-origin",
        "body": null,
        "method": "OPTIONS",
        "mode": "cors",
        "credentials": "omit"
      }).then(res => {
        //Selection image signature
        selectSignatureImg().then(base64Signature => {
          //POST signature Edusign
          fetch(`https://api.edusign.fr/student/courses/email/setStudentPresent/${courseId}?verificationToken=${verificationToken}`, {
            "headers": {
              "accept": "application/json, text/plain, */*",
              "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
              "content-type": "application/json;charset=UTF-8",
              "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"100\", \"Google Chrome\";v=\"100\"",
              "sec-ch-ua-mobile": "?0",
              "sec-ch-ua-platform": "\"Windows\"",
              "sec-fetch-dest": "empty",
              "sec-fetch-mode": "cors",
              "sec-fetch-site": "same-site"
            },
            "referrer": "https://static.edusign.fr/",
            "referrerPolicy": "strict-origin-when-cross-origin",
            "body": `{"studentId":"${studentId}","schoolId":"${schoolId}","base64Signature":"${base64Signature}"}`,
            "method": "POST",
            "mode": "cors",
            "credentials": "omit"
          }).then((res) => {
            return res.json()
          }).then(json=>{
            console.log(`${new Date(Date.now()).toISOString()}: Envoi de signature effectué. data={"status"="${json.status}","result"="${json.result}"}`)
          })
          .catch(err => console.log("error: ", console.log(err)));;

      })
      .catch(err => console.log("error: ", console.log(err)));
      });
    })
  })
}

// Setting a cron job
//Horaire cron par rapport au fuseau horaire UTC
cron.schedule("0 15 7 * * 1,2,3,4,5", function() {
  //A 9h15, envoie la signature entre 9h15 et 9h25 (UTC+2 heure d'été) (du lundi au vendredi)
  resolveDateBasedValues();
  setTimeout(sendSignature, Math.floor(Math.random() * 1000*60*10))
});

cron.schedule("0 45 11 * * 1,2,3,4,5", function() {
  //A 13h45, envoie la signature entre 13h45 et 13h55 (UTC+2 heure d'été) (du lundi au vendredi)
  resolveDateBasedValues();
  setTimeout(sendSignature, Math.floor(Math.random() * 1000*60*10))
});
