# run and detach
docker-compose up -d web

# save logs to a file in the background
docker-compose logs -f web > web.log 2>&1 &

# reattach to the container to be able to do control-c
# it will not rerun the container, just reattach
#docker-compose up web
