<div id="top"></div>


<br />
<div align="center">

<h3 align="center">node-signature-edusign</h3>

</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Un programme node qui vient signer sur AJC QUEST / Edusign en tache plannifié à 9h15 et 13h45.
Les signatures doivent êtres crées au préalable (400x200 .png) et placé dans le dossier /img/unused


### Built With

* [Node.js](https://nodejs.org/)
* [Docker](https://www.docker.com/)


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

Suivez les instructions suivantes.

### Prerequisites

Environment unix pour les commandes
Machine en horaire UTC (-2 heures par rapport à l'heure d'été)

### Installation

1. Clone the repo
   ```sh
   git clone git@gitlab.com:SebastienCHEVALIER/node-signature-edusign.git
   ```
2. Modifier student.json
   ```js
   {
     "civilite" : "M./Mme", <--M. ou Mme
     "nom" : "NOM", <--- Votre nom
     "prenom" : "PRENOM", <--- Votre prenom
     "mail" : "mail@example.com", <--- Votre mail
     "entreprise" : "THALES",  <--- Ne pas modifier
     "photo" : "photos/NOM_Prenom.jpg", <--- Photo sur AJC
     "filiere" : "F-220411-DIS-399-SOFTWARE_DEV_CPP_THALES", <--- Ne pas modifier
     "pc" : 0, <--- numéro de pc AJC ou 0
     "distance" : 1 <--- Ne pas modifier
   }


   ```
3. Remplir le dossier img/unused par vos images de signatures. https://sebastienchevalier.gitlab.io/ pour créer des images de signatures.

4. Installer Docker
    ```sh
    sudo apt install docker-compose
    ```

5. Lancer le container
    ```sh
    sudo docker-compose up web
    ```
    ou (pour enregistrer les log dans web.log et commande pour lancer le container en détacher)
    ```sh
    sudo ./run.sh
    ```

<p align="right">(<a href="#top">back to top</a>)</p>
